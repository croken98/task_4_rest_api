package by.zinkov.task4.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "resource_id";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).stateless(false);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER).and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/users").not().authenticated()
                .antMatchers(HttpMethod.DELETE, "/tours/upload").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/users/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/users/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, "/hotels/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/hotels/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/hotels/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/hotels/**").permitAll()
                .antMatchers("/tours/**/add_review").hasAnyAuthority("USER")
                .antMatchers("/tours/**/remove_from_favourite").hasAnyAuthority("USER")
                .antMatchers("/tours/**/add_to_favourite").hasAnyAuthority("USER")
                .antMatchers(HttpMethod.DELETE, "/tours/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/tours/**").permitAll()
                .antMatchers(HttpMethod.POST, "/tours/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/tours/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/countries/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/countries/**").permitAll()
                .antMatchers(HttpMethod.POST, "/countries/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/countries/**").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/reviews/add_review").hasAnyAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/profile/**").authenticated();
    }
}