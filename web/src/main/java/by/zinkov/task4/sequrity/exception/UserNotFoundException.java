package by.zinkov.task4.sequrity.exception;

public class UserNotFoundException extends IllegalArgumentException {
    public UserNotFoundException() {
        super("User not found!");
    }
}
