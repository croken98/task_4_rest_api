package by.zinkov.task4.sequrity.detail;

import by.zinkov.task4.bean.User;
import by.zinkov.task4.repository.impl.UserRepositoryImpl;
import by.zinkov.task4.sequrity.exception.UserNotFoundException;
import by.zinkov.task4.specification.impl.GetUserByLoginSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component("customDetailsService")
@ComponentScan(basePackageClasses = UserRepositoryImpl.class)
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepositoryImpl userRepository;
    @Autowired
    private GetUserByLoginSpecification specification;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        specification.setLogin(username);
        List<User> userCondidate = userRepository.query(specification);
        Optional<User> userOptional = !userCondidate.isEmpty() ? Optional.of(userCondidate.get(0)) : Optional.empty();
        return new UserDetailsImpl(
                userOptional.orElseThrow(UserNotFoundException::new)
        );
    }
}
