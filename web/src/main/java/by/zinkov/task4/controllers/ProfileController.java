package by.zinkov.task4.controllers;

import by.zinkov.task4.bean.User;
import by.zinkov.task4.convertors.ReviewReviewDtoConvertor;
import by.zinkov.task4.convertors.TourTourDtoConvertor;
import by.zinkov.task4.dto.ReviewDto;
import by.zinkov.task4.dto.TourDto;
import by.zinkov.task4.dto.UserDto;
import by.zinkov.task4.sequrity.exception.UserNotFoundException;
import by.zinkov.task4.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/profile")
@Api("Controller witch allows manipulate user profile. Only for authorized users!")
public class ProfileController {
    @Autowired
    private UserService<String> userService;
    @Autowired
    private TourTourDtoConvertor tourTourDtoConvertor;
    @Autowired
    private ReviewReviewDtoConvertor reviewReviewDtoConvertor;

    @ApiOperation("Get all tours witch user add to favourite!")
    @GetMapping("/tours")
    @ResponseStatus(HttpStatus.OK)
    public List<TourDto> getTours(Principal principal) {
        UserDto userDto = userService.getByLogin(principal.getName());
        User user = userService.getByPk(userDto.getId()).orElseThrow(UserNotFoundException::new);
        return user.getTours()
                .stream()
                .map(tour
                        -> tourTourDtoConvertor
                        .toDTO(tour))
                .collect(Collectors.toList());
    }

    @GetMapping("/reviews")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Get all reviews which user done. Allows  for authorized users!")
    public List<ReviewDto> getReviews(Principal principal) {
        UserDto userDto = userService.getByLogin(principal.getName());
        User user = userService.getByPk(userDto.getId()).orElseThrow(UserNotFoundException::new);
        return user.getReviews()
                .stream()
                .map(review
                        -> reviewReviewDtoConvertor
                        .toDTO(review))
                .collect(Collectors.toList());
    }
}
