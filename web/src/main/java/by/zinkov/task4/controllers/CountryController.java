package by.zinkov.task4.controllers;

import by.zinkov.task4.bean.Country;
import by.zinkov.task4.convertors.CountryCountryDtoConvertor;
import by.zinkov.task4.dto.CountryDto;
import by.zinkov.task4.exception.NotFountException;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.service.CountryService;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedCountriesSpecification;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("/countries")
@Api("Controller witch allows manipulate countries!")
public class CountryController {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private CountryService<String> countryService;
    @Autowired
    private CountryCountryDtoConvertor countryCountryDtoConvertor;

    @GetMapping(params = {"page", "size"})
    @ApiOperation("Get paginated countries with size equals size-parameter!")
    PairPaginated<List<CountryDto>, Long> getCountries(@RequestParam("page") int page,
                                                       @RequestParam("size") int size) {
        PaginatedSpecification<Country> specification = context.getBean(GetPaginatedCountriesSpecification.class);
        specification.setCount(size);
        specification.setPageNumber(page);
        PairPaginated<List<Country>, Long> listCountryLongPairPaginated = countryService.getPaginatedCountries(page, size);
        PairPaginated<List<CountryDto>, Long> pairPaginated = new PairPaginated<>();
        pairPaginated.setObjects(listCountryLongPairPaginated.getObjects().stream().map(country -> countryCountryDtoConvertor.toDTO(country)).collect(Collectors.toList()));
        pairPaginated.setCount(listCountryLongPairPaginated.getCount());
        return pairPaginated;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Remove country. Allows only for admin!")
    private void removeCountry(@PathVariable("id") String countryId) {
        countryService.getByPk(countryId).ifPresentOrElse(country -> countryService.remove(country)
                , () -> {
                    throw new IllegalArgumentException();
                });
    }


    @PutMapping("/{country_id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Update country. Allows only for admin!")
    private Country updateCountry(
            @RequestBody Country country
            , @PathVariable("country_id") String countryId) {
        countryService.getByPk(countryId).orElseThrow(()
                -> new NotFountException("Cannot find country by ID: " + countryId));
        country.setId(countryId);
        countryService.update(country);
        return country;
    }

    @GetMapping("/{country_id}")
    @ApiOperation("Get country. Allows only for all users!")
    private Country getCountry(@PathVariable("country_id") String countryId) {
        return countryService.getByPk(countryId).orElseThrow(()
                -> new NotFountException("Cannot find country by ID: " + countryId));
    }

    @PostMapping
    @ApiOperation("Add country. Allows only for admin!")
    public ResponseEntity<Object> addCountry(@RequestBody @Valid Country country, HttpServletRequest request) {
        Country saved = countryService.add(country).orElseThrow(RuntimeException::new);
        return created(
                ServletUriComponentsBuilder
                        .fromContextPath(request)
                        .path("/countries/{id}")
                        .buildAndExpand(saved.getId())
                        .toUri())
                .build();
    }
}
