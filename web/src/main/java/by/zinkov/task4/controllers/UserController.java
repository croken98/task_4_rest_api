package by.zinkov.task4.controllers;

import by.zinkov.exception.LoginAlreadyExistException;
import by.zinkov.task4.bean.User;
import by.zinkov.task4.dto.UserDto;
import by.zinkov.task4.exception.NotFountException;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.service.UserService;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedUsersSpecification;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("/users")
@Api("Controller witch allows manipulate users!")
public class UserController {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private UserService<String> userService;

    @GetMapping(params = {"page", "size"})
    @ApiOperation("Get paginated users with size equals size-parameter!")
    PairPaginated<List<UserDto>, Long> getUsers(
            @RequestParam("page") int page
            , @RequestParam("size") int size) {

        PaginatedSpecification<User> specification = context.getBean(GetPaginatedUsersSpecification.class);
        specification.setCount(size);
        specification.setPageNumber(page);
        return userService.getPaginatedUsers(page, size);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Remove user. Allows only for admin!")
    private void removeUser(@PathVariable("id") String userId) {
        userService.getByPk(userId).ifPresentOrElse(user -> userService.remove(user)
                , () -> {
                    throw new IllegalArgumentException();
                });
    }

    @GetMapping("/{user_id}")
    @ApiOperation("Get user profile by id. Allows for all users!")
    private User getUser(@PathVariable("user_id") String userId) {
        return userService.getByPk(userId).orElseThrow(()
                -> new NotFountException("Cannot find user by ID: " + userId));
    }

    @PostMapping
    @ApiOperation("Url for register users! Only for not-authorised users!")
    public ResponseEntity<Object> addUser(@RequestBody @Valid UserDto userDto, HttpServletRequest request) {
        try {
            UserDto saved = userService.registerNewUserAccount(userDto);
            return created(
                    ServletUriComponentsBuilder
                            .fromContextPath(request)
                            .path("/users/{id}")
                            .buildAndExpand(saved.getId())
                            .toUri())
                    .build();
        } catch (LoginAlreadyExistException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
