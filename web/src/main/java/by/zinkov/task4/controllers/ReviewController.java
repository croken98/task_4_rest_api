package by.zinkov.task4.controllers;

import by.zinkov.task4.bean.Review;
import by.zinkov.task4.exception.NotFountException;
import by.zinkov.task4.service.impl.ReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reviews")
@Api("Controller witch allows manipulate reviews!")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{review_id}")
    @ApiOperation("Delete review. Allows only for authorized")
    public void removeReview(@PathVariable("review_id") String reviewId) {
        Review review = reviewService.getByPk(reviewId).orElseThrow(() -> new NotFountException("Cannot find review by ID: " + reviewId));
        reviewService.remove(review);
    }
}
