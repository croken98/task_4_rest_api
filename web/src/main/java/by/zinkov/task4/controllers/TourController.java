package by.zinkov.task4.controllers;

import by.zinkov.task4.bean.Review;
import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.bean.User;
import by.zinkov.task4.convertors.ReviewReviewDtoConvertor;
import by.zinkov.task4.convertors.TourTourDtoConvertor;
import by.zinkov.task4.dto.ReviewDto;
import by.zinkov.task4.dto.TourDto;
import by.zinkov.task4.dto.UserDto;
import by.zinkov.task4.exception.NotFountException;
import by.zinkov.task4.exception.FileUploadFileException;
import by.zinkov.task4.form.FileForm;
import by.zinkov.task4.json.TourJSONParserFromFile;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.sequrity.exception.UserNotFoundException;
import by.zinkov.task4.service.TourService;
import by.zinkov.task4.service.UserService;
import by.zinkov.task4.service.impl.ReviewService;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedToursSpecification;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("/tours")
@Api("Controller witch allows manipulate tours!")
public class TourController {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private TourService<String> tourService;
    @Autowired
    private UserService<String> userService;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private ReviewReviewDtoConvertor reviewReviewDtoConvertor;
    @Autowired
    private TourTourDtoConvertor tourTourDtoConvertor;

    @GetMapping(params = {"page", "size"})
    @ApiOperation("Get paginated tours with size equals size-parameter!")
    PairPaginated<List<TourDto>, Long> getTours(@RequestParam("page") int page,
                                                @RequestParam("size") int size) {
        PaginatedSpecification<Tour> specification = context.getBean(GetPaginatedToursSpecification.class);
        specification.setCount(size);
        specification.setPageNumber(page);
        PairPaginated<List<Tour>, Long> listTourLongPairPaginated = tourService.getPaginatedTours(page, size);
        PairPaginated<List<TourDto>, Long> pairPaginated = new PairPaginated<>();
        pairPaginated.setObjects(listTourLongPairPaginated.getObjects().stream().map(hotel -> tourTourDtoConvertor.toDTO(hotel)).collect(Collectors.toList()));
        pairPaginated.setCount(listTourLongPairPaginated.getCount());
        return pairPaginated;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Remove tour. Allows only for admin!")
    private void removeTour(@PathVariable("id") String userId) {
        tourService.getByPk(userId).ifPresentOrElse(user -> tourService.remove(user)
                , () -> {
                    throw new IllegalArgumentException();
                });
    }


    @PutMapping("/{tour_id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Update tour. Allows only for admin!")
    private Tour updateTour(
            @RequestBody Tour tour
            , @PathVariable("tour_id") String tourId) {
        tourService.getByPk(tourId).orElseThrow(() -> new NotFountException("cannot find tour by ID: " + tourId));
        tour.setId(tourId);
        tourService.update(tour);
        return tour;
    }

    @GetMapping("/{tour_id}")
    @ApiOperation("Get tour by ID. Allows for all users!")
    private TourDto getTour(@PathVariable("tour_id") String tourId) {
        return tourTourDtoConvertor.toDTO(tourService
                .getByPk(tourId)
                .orElseThrow(() -> new NotFountException("cannot find tour by ID: " + tourId)));
    }

    @PostMapping
    @ApiOperation("Add tour. Allows only for admin!")
    public ResponseEntity<Object> addTour(@RequestBody @Valid Tour tour, HttpServletRequest request) {
        Tour saved = tourService.add(tour).orElseThrow(RuntimeException::new);
        return created(
                ServletUriComponentsBuilder
                        .fromContextPath(request)
                        .path("/tours/{id}")
                        .buildAndExpand(saved.getId())
                        .toUri())
                .build();
    }

    @PutMapping("/{tour_id}/add_to_favourite")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Add tour to favourite for authorized user which use this url!")
    public void addTourToFavourite(@PathVariable("tour_id") String tourId, Principal principal) {
        UserDto userDto = userService.getByLogin(principal.getName());
        User user = userService.getByPk(userDto.getId()).orElseThrow(UserNotFoundException::new);
        Tour tour = tourService.getByPk(tourId).orElseThrow(RuntimeException::new);
        user.getTours().add(tour);
        userService.update(user);
    }

    @DeleteMapping("/{tour_id}/remove_from_favourite")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Remove tour from favourite for authorized user which use this url!")
    public void removeTourFromFavourite(@PathVariable("tour_id") String tourId, Principal principal) {
        UserDto userDto = userService.getByLogin(principal.getName());
        User user = userService.getByPk(userDto.getId()).orElseThrow(UserNotFoundException::new);
        Tour tour = tourService.getByPk(tourId).orElseThrow(RuntimeException::new);
        user.getTours().remove(tour);
        userService.update(user);
    }

    @PostMapping("/{tour_id}/add_review")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Add review to this tour which ID in URL, only for USER")
    public void addReview(@RequestBody Review review, @PathVariable("tour_id") String tourId, Principal principal) {
        UserDto userDto = userService.getByLogin(principal.getName());
        User user = userService.getByPk(userDto.getId()).orElseThrow(UserNotFoundException::new);
        Tour tour = tourService.getByPk(tourId).orElseThrow(RuntimeException::new);
        review.setUser(user);
        review.setTour(tour);
        review.setDate(new Timestamp(new Date().getTime()));
        reviewService.add(review);
        tour.getReviews().add(review);
        user.getReviews().add(review);
        tourService.update(tour);
        userService.update(user);
    }

    @GetMapping("/{tour_id}/reviews")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Get all reviews for current tour! Allows for all users!")
    public List<ReviewDto> getTourReviews(@PathVariable("tour_id") String tourId) {
        Tour tour = tourService.getByPk(tourId).orElseThrow(RuntimeException::new);
        return tour
                .getReviews()
                .stream()
                .map(review ->
                        reviewReviewDtoConvertor
                                .toDTO(review))
                .collect(Collectors.toList());
    }

    @GetMapping("/{tour_id}/users")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Get all users which add  current tour to favourite! Allows for Users!")
    public List<User> getTourUsers(@PathVariable("tour_id") String tourId) {
        Tour tour = tourService.getByPk(tourId).orElseThrow(RuntimeException::new);
        return tour.getUsers();
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/upload")
    public void butchDownloadingTours(@Valid FileForm file) {
        MultipartFile realFile = file.getFileTours();
        if (!realFile.isEmpty()) {
            TourJSONParserFromFile parser = new TourJSONParserFromFile();
            List<Tour> tours;
            try {
                Reader reader = new InputStreamReader(realFile.getInputStream());
                tours = parser.parse(reader);
                tourService.insertBath(tours);
            } catch (Exception e) {
                throw new FileUploadFileException(e);
            }
        }
    }
}
