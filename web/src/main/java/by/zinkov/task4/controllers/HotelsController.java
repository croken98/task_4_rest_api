package by.zinkov.task4.controllers;

import by.zinkov.task4.bean.Hotel;
import by.zinkov.task4.convertors.HotelHotelDtoConvertor;
import by.zinkov.task4.dto.HotelDto;
import by.zinkov.task4.exception.NotFountException;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.service.HotelService;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedHotelsSpecification;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("/hotels")
@Api("Controller witch allows manipulate hotels!")
public class HotelsController {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private HotelService<String> hotelService;
    @Autowired
    private HotelHotelDtoConvertor hotelHotelDtoConvertor;

    @GetMapping(params = {"page", "size"})
    @ApiOperation("Get paginated hotels with size equals size-parameter!")
    PairPaginated<List<HotelDto>, Long> getHotels(@RequestParam("page") int page,
                                               @RequestParam("size") int size) {
        PaginatedSpecification<Hotel> specification = context.getBean(GetPaginatedHotelsSpecification.class);
        specification.setCount(size);
        specification.setPageNumber(page);
        PairPaginated<List<Hotel>, Long> listHotelLongPairPaginated = hotelService.getPaginatedHotels(page, size);
        PairPaginated<List<HotelDto>, Long> pairPaginated = new PairPaginated<>();
        pairPaginated.setObjects(listHotelLongPairPaginated.getObjects().stream().map(hotel -> hotelHotelDtoConvertor.toDTO(hotel)).collect(Collectors.toList()));
        pairPaginated.setCount(listHotelLongPairPaginated.getCount());
        return  pairPaginated;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Remove hotel. Allows only for admin!")
    private void removeHotel(@PathVariable("id") String userId) {
        hotelService.getByPk(userId).ifPresentOrElse(user -> hotelService.remove(user)
                , () -> {
                    throw new IllegalArgumentException();
                });
    }


    @PutMapping("/{hotel_id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Update hotel. Allows only for admin!")
    private Hotel updateHotel(
            @RequestBody Hotel hotel
            , @PathVariable("hotel_id") String hotelId) {
        hotelService.getByPk(hotelId).orElseThrow(() -> new NotFountException("Cannot find hotel by ID: " + hotelId));
        hotel.setId(hotelId);
        hotelService.update(hotel);
        return hotel;
    }

    @GetMapping("/{hotel_id}")
    @ApiOperation("Get hotel by ID. Allows for all users!")
    private Hotel getHotel(@PathVariable("hotel_id") String hotelId) {
        return hotelService.getByPk(hotelId).orElseThrow(()
                -> new NotFountException("Cannot find hotel by ID: " + hotelId)
        );
    }

    @PostMapping
    @ApiOperation("Add hotel. Allows only for  admin!")
    public ResponseEntity<Object> addHotel(@RequestBody @Valid Hotel hotel, HttpServletRequest request) {
        Hotel saved = hotelService.add(hotel).orElseThrow(RuntimeException::new);
        return created(
                ServletUriComponentsBuilder
                        .fromContextPath(request)
                        .path("/hotels/{id}")
                        .buildAndExpand(saved.getId())
                        .toUri())
                .build();
    }
}
