package by.zinkov.task4.form;

import by.zinkov.task4.validation.JSONFileConstraint;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FileForm {
    @JSONFileConstraint
    private MultipartFile fileTours;

}
