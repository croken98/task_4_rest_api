package by.zinkov.task4.exception;

public class FileUploadFileException extends RuntimeException {
    public FileUploadFileException(Throwable cause) {
        super("Problem with file!", cause);
    }
}
