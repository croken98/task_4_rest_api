package by.zinkov.task4.exception;

public class NotFountException extends RuntimeException {
    public NotFountException(String message) {
        super(message);
    }
}
