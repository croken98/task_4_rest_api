package by.zinkov.task4.specification;

import by.zinkov.repository.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public abstract class MongoDBSpecification<T>  implements Specification<T> {
    @Autowired
    protected MongoRepository<T,String> mongoRepository;
}
