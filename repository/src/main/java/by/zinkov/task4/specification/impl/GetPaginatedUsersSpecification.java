package by.zinkov.task4.specification.impl;

import by.zinkov.task4.bean.User;
import by.zinkov.task4.specification.AbstractPaginatedSpecification;
import org.springframework.stereotype.Component;

@Component
public class GetPaginatedUsersSpecification extends AbstractPaginatedSpecification<User> {
    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }
}
