package by.zinkov.task4.specification.impl;

import by.zinkov.task4.bean.Country;
import by.zinkov.task4.specification.AbstractPaginatedSpecification;
import org.springframework.stereotype.Component;

@Component
public class GetPaginatedCountriesSpecification extends AbstractPaginatedSpecification<Country> {
    @Override
    protected Class<Country> getEntityClass() {
        return Country.class;
    }
}

