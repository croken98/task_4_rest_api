package by.zinkov.task4.specification.impl;

import by.zinkov.task4.bean.Hotel;
import by.zinkov.task4.specification.AbstractPaginatedSpecification;
import org.springframework.stereotype.Component;

@Component
public class GetPaginatedHotelsSpecification extends AbstractPaginatedSpecification<Hotel> {
    @Override
    protected Class<Hotel> getEntityClass() {
        return Hotel.class;
    }
}
