package by.zinkov.task4.specification;

import java.util.List;

public abstract class GetAllSpecification<T> extends MongoDBSpecification<T> implements by.zinkov.specification.GetAllSpecification<T> {
    @Override
    public List<T> execute() {
        return mongoRepository.findAll();
    }
}
