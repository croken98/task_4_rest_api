package by.zinkov.task4.specification;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
@Scope("prototype")
public abstract class AbstractPaginatedSpecification<T> implements PaginatedSpecification<T> {

    @Autowired
    private MongoTemplate mongoTemplate;

    protected abstract Class<T> getEntityClass();

    @Getter
    @Setter
    private Integer pageNumber;
    @Getter
    @Setter
    private Integer count;

    @Override
    public List<T> execute() {
        final Pageable pageableRequest = PageRequest.of(pageNumber, count);
        Query query = new Query();
        query.with(pageableRequest);
        return mongoTemplate.find(query, getEntityClass());
    }
}
