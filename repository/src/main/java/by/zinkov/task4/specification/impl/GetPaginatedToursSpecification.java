package by.zinkov.task4.specification.impl;

import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.specification.AbstractPaginatedSpecification;
import org.springframework.stereotype.Component;

@Component
public class GetPaginatedToursSpecification extends AbstractPaginatedSpecification<Tour> {
    @Override
    protected Class<Tour> getEntityClass() {
        return Tour.class;
    }
}
