package by.zinkov.task4.specification.impl;

import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.specification.GetAllSpecification;
import org.springframework.stereotype.Component;

@Component
public class GetAllTours extends GetAllSpecification<Tour> {
}
