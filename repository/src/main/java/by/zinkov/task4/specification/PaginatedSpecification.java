package by.zinkov.task4.specification;

import by.zinkov.repository.Specification;

public interface PaginatedSpecification<T> extends Specification<T> {
    void setCount(Integer count);

    Integer getCount();

    void setPageNumber(Integer pageNumber);

    Integer getPageNumber();
}
