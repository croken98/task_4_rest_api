package by.zinkov.task4.specification.impl;

import by.zinkov.task4.bean.User;
import by.zinkov.task4.repository.UserRepository;
import by.zinkov.task4.specification.MongoDBSpecification;

import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@Scope("prototype")
public class GetUserByLoginSpecification extends MongoDBSpecification<User> {

    @Setter
    private String login;

    @Override
    public List<User> execute() {
        UserRepository userRepository = (UserRepository) mongoRepository;
        return Collections.singletonList(userRepository.findByLogin(login));
    }
}
