package by.zinkov.task4.repository;

import by.zinkov.task4.bean.Review;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

public interface ReviewRepository extends MongoRepository<Review, String> {
}
