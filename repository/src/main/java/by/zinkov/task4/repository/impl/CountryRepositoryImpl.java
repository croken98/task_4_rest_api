package by.zinkov.task4.repository.impl;

import by.zinkov.task4.bean.Country;
import by.zinkov.task4.repository.AbstractMongodbRepository;

public class CountryRepositoryImpl extends AbstractMongodbRepository<Country> {
}
