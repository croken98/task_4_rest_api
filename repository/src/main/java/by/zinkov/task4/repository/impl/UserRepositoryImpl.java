package by.zinkov.task4.repository.impl;

import by.zinkov.task4.bean.User;
import by.zinkov.task4.repository.AbstractMongodbRepository;
import org.springframework.stereotype.Component;

@Component
public class UserRepositoryImpl extends AbstractMongodbRepository<User> {

}
