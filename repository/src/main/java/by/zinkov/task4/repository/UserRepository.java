package by.zinkov.task4.repository;

import by.zinkov.task4.bean.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

public interface UserRepository extends MongoRepository<User, String> {
    User findByLogin(String login);
}
