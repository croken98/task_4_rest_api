package by.zinkov.task4.repository;

import by.zinkov.task4.bean.Tour;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

public interface TourRepository extends MongoRepository<Tour, String> {
}
