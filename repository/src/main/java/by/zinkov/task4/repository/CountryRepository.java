package by.zinkov.task4.repository;

import by.zinkov.task4.bean.Country;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

public interface CountryRepository extends MongoRepository<Country, String> {
}
