package by.zinkov.task4.repository.impl;

import by.zinkov.task4.bean.Review;
import by.zinkov.task4.repository.AbstractMongodbRepository;

public class ReviewRepositoryImpl extends AbstractMongodbRepository<Review> {
}
