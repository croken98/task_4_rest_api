package by.zinkov.task4.repository;

import by.zinkov.bean.Identity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public abstract class AbstractMongodbRepository<T extends Identity<String>> implements by.zinkov.repository.Repository<T, String> {

    @Autowired
    private MongoRepository<T, String> mongoRepository;

    @Override
    public void insertBatch(List<T> objects, int batchSize) {
        mongoRepository.saveAll(objects);
    }

    @Override
    public Optional<T> save(T object) {
        return Optional.of(mongoRepository.save(object));
    }

    @Override
    public void delete(T object) {
        mongoRepository.delete(object);
    }

    @Override
    public void update(T object) {
        if (object.getId() == null) {
            throw new IllegalStateException("Object don't located in db");
        }
        mongoRepository.save(object);
    }

    @Override
    public Optional<T> getByPK(String id) {
        return mongoRepository.findById(id);
    }

    @Override
    public long getCount() {
        return mongoRepository.count();
    }

    @Override
    public List<T> query(by.zinkov.repository.Specification<T> specification) {
        return specification.execute();
    }
}
