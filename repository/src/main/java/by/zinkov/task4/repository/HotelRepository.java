package by.zinkov.task4.repository;

import by.zinkov.task4.bean.Hotel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HotelRepository extends MongoRepository<Hotel, String > {
}
