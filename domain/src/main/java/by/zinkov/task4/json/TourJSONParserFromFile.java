package by.zinkov.task4.json;

import by.zinkov.json.EntityParser;
import by.zinkov.task4.bean.Country;
import by.zinkov.task4.bean.Hotel;
import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.bean.TourType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Scope("prototype")
public class TourJSONParserFromFile implements EntityParser<Tour> {
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public List<Tour> parse(Reader reader) throws IOException, ParseException, org.json.simple.parser.ParseException {
        JSONParser parser = new JSONParser();
        JSONArray jsonArray = (JSONArray) parser.parse(reader);
        List<Tour> tours = new ArrayList<>();
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            Tour tour = new Tour();
            setPhoto(tour, jsonObject);
            setCost(tour, jsonObject);
            setCountry(tour, jsonObject);
            tour.setDescription((String) jsonObject.get("description"));
            tour.setDuration(((Long) jsonObject.get("duration")).intValue());
            setHotel(tour, jsonObject);
            tour.setTour(TourType.valueOf((String) jsonObject.get("tour")));
            tours.add(tour);
        }
        return tours;
    }

    private void setPhoto(Tour tour, JSONObject object) throws java.text.ParseException {
        tour.setPhoto((String) object.get("photo"));
        Date date = FORMATTER.parse((String) object.get("date"));
        tour.setDate(date);
    }

    private void setCost(Tour tour, JSONObject jsonObject) {
        BigDecimal cost = BigDecimal.valueOf((Double) jsonObject.get("cost"));
        tour.setCost(cost);
    }

    private void setCountry(Tour tour, JSONObject jsonObject) {
        Country country = new Country();
        country.setId(((String) jsonObject.get("country_id")));
        tour.setCountry(country);
    }

    private void setHotel(Tour tour, JSONObject jsonObject) {
        Hotel hotel = new Hotel();
        hotel.setId(((String) jsonObject.get("hotel_id")));
        tour.setHotel(hotel);
    }
}
