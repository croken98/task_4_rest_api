package by.zinkov.task4.bean;

import by.zinkov.bean.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Document(collection = "reviews")
public class Review implements Identity<String> {
    private static final int MAX_TEXT_LENGTH = 1000;

    @Override
    public String getId() {
        return id;
    }

    @Id
    private String id;

    @NotNull
    @NotEmpty
    @Size(min = 1, max = MAX_TEXT_LENGTH)
    private String text;

    private Date date;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private User user;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private Tour tour;
}
