package by.zinkov.task4.bean;

import by.zinkov.bean.Identity;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Document(collection = "countries")
public class Country implements Identity<String> {
    private static final int MAX_NAME_LENGTH = 40;

    @Id
    private String id;

    @NotNull
    @Size(max = MAX_NAME_LENGTH)
    private String name;

    @Override
    public String getId() {
        return id;
    }
}
