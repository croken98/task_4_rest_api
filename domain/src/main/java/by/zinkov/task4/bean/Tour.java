package by.zinkov.task4.bean;

import by.zinkov.bean.Identity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Data

@Document(collection = "tours")
public class Tour implements Identity<String> {
    private static final int MAX_DURATION_LENGTH = 365;
    private static final int MAX_DESCRIPTION_LENGTH = 1000;
    private static final int MAX_DECIMAL_SIZE = 100000;
    private static final int MAX_PHOTO_LENGTH = 400;

    @Override
    public String getId() {
        return id;
    }

    @Id
    private String id;

    @Size(max = MAX_PHOTO_LENGTH)
    private String photo;

    private Date date;

    @NotNull
    @Max(MAX_DURATION_LENGTH)
    private long duration;

    @NotNull
    @Size(max = MAX_DESCRIPTION_LENGTH)
    private String description;

    @NotNull
    @Max(MAX_DECIMAL_SIZE)
    private BigDecimal cost;

    private TourType tour;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private Hotel hotel;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private Country country;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private List<User> users = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private List<Review> reviews = new ArrayList<>();
}
