package by.zinkov.task4.bean;

import by.zinkov.bean.Identity;
import lombok.Data;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Document(collection = "users")
public class User implements Identity<String> {

    @Override
    public String getId() {
        return id;
    }

    @Id
    private String id;

    private String login;

    private String password;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private List<Tour> tours = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DBRef(lazy = true)
    private List<Review> reviews = new ArrayList<>();

    @NotNull
    private Role role;
}
