package by.zinkov.task4.pair;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PairPaginated<T, P> {
    private T objects;
    private P count;
}
