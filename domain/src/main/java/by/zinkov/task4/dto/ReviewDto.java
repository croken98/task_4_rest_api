package by.zinkov.task4.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
public class ReviewDto {
    private static final int MAX_TEXT_LENGTH = 1000;

    @Id
    private String id;

    @NotNull
    @NotEmpty
    @Size(min = 1, max = MAX_TEXT_LENGTH)
    private String text;

    private Date date;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private UserDto user;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private TourDto tour;
}
