package by.zinkov.task4.dto;

import by.zinkov.task4.bean.Role;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserDto {
    private static final int MAX_LOGIN_LENGTH = 45;
    private static final int PASSWORD_LENGTH = 64;

    private String id;
    @Size(max = MAX_LOGIN_LENGTH)
    @NotNull
    @NotEmpty
    private String login;

    @Size(max = PASSWORD_LENGTH)
    @NotNull
    @NotEmpty
    private String password;

    private Role role;
}
