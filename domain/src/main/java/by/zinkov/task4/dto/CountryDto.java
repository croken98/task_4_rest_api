package by.zinkov.task4.dto;

import by.zinkov.bean.Identity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CountryDto implements Identity<String> {
    private static final int MAX_NAME_LENGTH = 40;
    private String id;

    @NotNull
    @Size(max = MAX_NAME_LENGTH)
    private String name;

    @Override
    public String getId() {
        return id;
    }
}
