package by.zinkov.task4.dto;

import by.zinkov.bean.Features;
import by.zinkov.bean.Identity;
import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class HotelDto implements Identity<String> {
    private static final int MAX_NAME_LENGTH = 45;
    private static final int MAX_STARS_LENGTH = 5;
    private static final int MAX_WEBSITE_LENGTH = 100;
    private static final int MAX_COORDINATES_LENGTH = 100;

    @Override
    public String getId() {
        return id;
    }

    @Id
    private String id;

    @NotNull
    @Size(max = MAX_NAME_LENGTH)
    private String name;

    @NotNull
    @Max(MAX_STARS_LENGTH)
    private byte stars;

    @NotNull
    @Size(max = MAX_WEBSITE_LENGTH)
    private String website;

    @NotNull
    @Size(max = MAX_COORDINATES_LENGTH)
    private String lalitude;

    @NotNull
    @Size(max = MAX_COORDINATES_LENGTH)
    private String longitude;

    private Features[] features;
}
