package by.zinkov.task4.dto;

import by.zinkov.task4.bean.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class TourDto {
    private static final int MAX_DURATION_LENGTH = 365;
    private static final int MAX_DESCRIPTION_LENGTH = 1000;
    private static final int MAX_DECIMAL_SIZE = 100000;
    private static final int MAX_PHOTO_LENGTH = 400;

    private String id;

    @Size(max = MAX_PHOTO_LENGTH)
    private String photo;

    private Date date;

    @NotNull
    @Max(MAX_DURATION_LENGTH)
    private long duration;

    @NotNull
    @Size(max = MAX_DESCRIPTION_LENGTH)
    private String description;

    @NotNull
    @Max(MAX_DECIMAL_SIZE)
    private BigDecimal cost;

    private TourType tour;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Hotel hotel;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Country country;
}
