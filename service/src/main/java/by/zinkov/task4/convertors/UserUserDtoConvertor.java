package by.zinkov.task4.convertors;

import by.zinkov.task4.bean.User;
import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.task4.dto.UserDto;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserUserDtoConvertor implements EntityDtoConvertor<User, UserDto> {

    @Override
    public UserDto toDTO(User user) {
        if (user == null) {
            return null;
        }
        UserDto userDto = new UserDto();
        userDto.setLogin(user.getLogin());
        userDto.setId(user.getId());
        userDto.setRole(user.getRole());
        return userDto;
    }

    @Override
    public User toEntity(UserDto userDto) {
        if (userDto == null) {
            return null;
        }
        User user = new User();
        user.setLogin(userDto.getLogin());
        user.setId(userDto.getId());

        if (userDto.getPassword() != null) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String encodePassword = encoder.encode(userDto.getPassword());
            user.setPassword(encodePassword);
        }
        user.setRole(userDto.getRole());
        return user;
    }
}
