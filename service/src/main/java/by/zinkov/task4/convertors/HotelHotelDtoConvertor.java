package by.zinkov.task4.convertors;

import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.task4.bean.Hotel;
import by.zinkov.task4.dto.HotelDto;
import org.springframework.stereotype.Component;

@Component
public class HotelHotelDtoConvertor implements EntityDtoConvertor<Hotel, HotelDto> {
    @Override
    public HotelDto toDTO(Hotel hotel) {
        if (hotel == null) {
            return null;
        }
        HotelDto hotelDto = new HotelDto();
        hotelDto.setId(hotel.getId());
        hotelDto.setLalitude(hotel.getLalitude());
        hotelDto.setLongitude(hotel.getLongitude());
        hotelDto.setFeatures(hotel.getFeatures());
        hotelDto.setName(hotel.getName());
        hotelDto.setStars(hotel.getStars());
        hotelDto.setWebsite(hotel.getWebsite());
        return hotelDto;
    }

    @Override
    public Hotel toEntity(HotelDto hotelDto) {
        throw new UnsupportedOperationException();
    }
}
