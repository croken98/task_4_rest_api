package by.zinkov.task4.convertors;

import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.task4.bean.Review;
import by.zinkov.task4.dto.ReviewDto;
import by.zinkov.task4.dto.TourDto;
import by.zinkov.task4.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReviewReviewDtoConvertor implements EntityDtoConvertor<Review, ReviewDto> {
    @Autowired
    private UserUserDtoConvertor userUserDtoConvertor;
    @Autowired
    private TourTourDtoConvertor tourTourDtoConvertor;

    @Override
    public ReviewDto toDTO(Review review) {
        if (review == null) {
            return null;
        }
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setId(review.getId());
        reviewDto.setDate(review.getDate());
        reviewDto.setText(review.getText());
        TourDto tour = tourTourDtoConvertor.toDTO(review.getTour());
        reviewDto.setTour(tour);
        UserDto userDto = userUserDtoConvertor.toDTO(review.getUser());
        reviewDto.setUser(userDto);
        return reviewDto;
    }

    @Override
    public Review toEntity(ReviewDto reviewDto) {
        throw new UnsupportedOperationException();

    }
}
