package by.zinkov.task4.convertors;

import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.task4.bean.Country;
import by.zinkov.task4.dto.CountryDto;
import org.springframework.stereotype.Component;

@Component
public class CountryCountryDtoConvertor implements EntityDtoConvertor<Country, CountryDto> {
    @Override
    public CountryDto toDTO(Country country) {
        if (country == null) {
            return null;
        }
        CountryDto countryDto = new CountryDto();
        countryDto.setId(country.getId());
        countryDto.setName(country.getName());
        return countryDto;
    }

    @Override
    public Country toEntity(CountryDto countryDto) {
        throw new UnsupportedOperationException();
    }
}
