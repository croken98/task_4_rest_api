package by.zinkov.task4.convertors;

import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.dto.TourDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TourTourDtoConvertor implements EntityDtoConvertor<Tour, TourDto> {
    @Autowired
    private ReviewReviewDtoConvertor reviewReviewDtoConvertor;
    @Autowired
    private UserUserDtoConvertor userUserDtoConvertor;

    @Override
    public TourDto toDTO(Tour tour) {
        if (tour == null) {
            return null;
        }
        TourDto tourDto = new TourDto();
        tourDto.setId(tour.getId());
        tourDto.setPhoto(tour.getPhoto());
        tourDto.setTour(tour.getTour());
        tourDto.setCost(tour.getCost());
        tourDto.setCountry(tour.getCountry());
        tourDto.setDate(tour.getDate());
        tourDto.setDescription(tour.getDescription());
        tourDto.setDuration(tour.getDuration());
        tourDto.setHotel(tour.getHotel());
        return tourDto;
    }

    @Override
    public Tour toEntity(TourDto tourDto) {
        throw new UnsupportedOperationException();

    }
}
