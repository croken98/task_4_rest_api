package by.zinkov.task4.service;

import by.zinkov.service.Service;
import by.zinkov.task4.bean.Country;
import by.zinkov.task4.pair.PairPaginated;

import java.util.List;

public interface CountryService<PK> extends Service<Country, PK> {
    PairPaginated<List<Country>, Long> getPaginatedCountries(int pageNumber, int countPerPage);
}
