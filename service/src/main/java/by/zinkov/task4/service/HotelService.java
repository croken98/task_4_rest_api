package by.zinkov.task4.service;

import by.zinkov.task4.bean.Hotel;
import by.zinkov.service.Service;
import by.zinkov.task4.pair.PairPaginated;

import java.util.List;

public interface HotelService<PK> extends Service<Hotel, PK>  {
    PairPaginated<List<Hotel>, Long> getPaginatedHotels(int pageNumber, int countPerPage);
}
