package by.zinkov.task4.service;

import by.zinkov.task4.bean.Review;
import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.bean.User;
import by.zinkov.task4.dto.UserDto;
import by.zinkov.exception.LoginAlreadyExistException;
import by.zinkov.service.Service;

import by.zinkov.task4.pair.PairPaginated;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserService<PK> extends Service<User, PK> {
    @Transactional("transactionManager")
    public List<Review> getUserReviews(UserDto userDto);

    @Transactional("transactionManager")
    public List<Tour> getUserTours(UserDto userDto);

    @Transactional("transactionManager")
    void addTour(UserDto userDto, Tour tour);

    UserDto getByLogin(String login);

    @Transactional("transactionManager")
    UserDto registerNewUserAccount(UserDto userDto) throws LoginAlreadyExistException;

    @Transactional
    PairPaginated<List<UserDto>, Long> getPaginatedUsers(int page, int size);
    @Transactional
    boolean hasThisTour(UserDto userDto, String tourId);

    @Transactional
    void removeTourFromUser(UserDto userDto, String tourId);
}
