package by.zinkov.task4.service.impl;

import by.zinkov.service.ServiceAbstract;
import by.zinkov.task4.bean.Country;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedCountriesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl extends ServiceAbstract<Country,String> implements by.zinkov.task4.service.CountryService<String > {
    @Autowired
    private ApplicationContext context;
    @Override
    public PairPaginated<List<Country>, Long> getPaginatedCountries(int pageNumber, int countPerPage) {
        PaginatedSpecification<Country> specification = context.getBean(GetPaginatedCountriesSpecification.class);
        specification.setCount(countPerPage);
        specification.setPageNumber(pageNumber);
        List<Country> countries = repository.query(specification);
        Long countCountries = repository.getCount();
        PairPaginated<List<Country>, Long> countriesCountPair = new PairPaginated<>();
        countriesCountPair.setObjects( countries);
        countriesCountPair.setCount(countCountries);
        return countriesCountPair;
    }
}
