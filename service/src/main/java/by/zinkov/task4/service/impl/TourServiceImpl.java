package by.zinkov.task4.service.impl;

import by.zinkov.service.ServiceAbstract;
import by.zinkov.task4.bean.Review;
import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedToursSpecification;
import by.zinkov.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TourServiceImpl extends ServiceAbstract<Tour, String> implements by.zinkov.task4.service.TourService<String> {
    @Autowired
    private ApplicationContext context;

    @Transactional
    public List<Review> getTourReviews(Tour tour) {
        tour = repository.getByPK(tour.getId()).get();
        tour.getReviews().size();
        return tour.getReviews();
    }

    @Transactional
    public PairPaginated<List<Tour>, Long> getPaginatedTours(int pageNumber, int countPerPage) {
        PaginatedSpecification<Tour> specification = context.getBean(GetPaginatedToursSpecification.class);
        specification.setCount(countPerPage);
        specification.setPageNumber(pageNumber);
        List<Tour> tours = repository.query(specification);
        Long countTours = repository.getCount();
        PairPaginated<List<Tour>, Long> toursCountPair = new PairPaginated<>();
        toursCountPair.setObjects( tours);
        toursCountPair.setCount(countTours);
        return toursCountPair;
    }
}
