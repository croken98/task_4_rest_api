package by.zinkov.task4.service.impl;

import by.zinkov.service.ServiceAbstract;
import by.zinkov.task4.bean.Review;
import org.springframework.stereotype.Service;

@Service
public class ReviewService extends ServiceAbstract<Review, String> {
}
