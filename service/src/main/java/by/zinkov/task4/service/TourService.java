package by.zinkov.task4.service;

import by.zinkov.task4.bean.Review;
import by.zinkov.task4.bean.Tour;
import by.zinkov.service.Service;
import by.zinkov.task4.pair.PairPaginated;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TourService<PK> extends Service<by.zinkov.task4.bean.Tour, PK> {
    @Transactional
    List<Review> getTourReviews(Tour tour);

    @Transactional
    PairPaginated<List<Tour>, Long> getPaginatedTours(int pageNumber, int countPerPage);
}
