package by.zinkov.task4.service.impl;

import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.task4.bean.Role;
import by.zinkov.task4.dto.UserDto;
import by.zinkov.exception.LoginAlreadyExistException;
import by.zinkov.service.ServiceAbstract;
import by.zinkov.task4.bean.Review;
import by.zinkov.task4.bean.Tour;
import by.zinkov.task4.bean.User;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedUsersSpecification;
import by.zinkov.task4.specification.impl.GetUserByLoginSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl extends ServiceAbstract<User, String> implements by.zinkov.task4.service.UserService<String> {
    @Autowired
    private EntityDtoConvertor<User, UserDto> userDtoEntityConverter;

    @Autowired
    ApplicationContext context;

    @Override
    public PairPaginated<List<UserDto>, Long> getPaginatedUsers(int page, int size) {
        PaginatedSpecification<User> specification = context.getBean(GetPaginatedUsersSpecification.class);
        specification.setCount(size);
        specification.setPageNumber(page);
        List<User> users = repository.query(specification);
        List<UserDto> userDtos = users.stream()
                .map(user -> userDtoEntityConverter.toDTO(user))
                .collect(Collectors.toList());
        Long countUsers = repository.getCount();
        PairPaginated<List<UserDto>, Long> userCountPair = new PairPaginated<>();
        userCountPair.setObjects( userDtos);
        userCountPair.setCount(countUsers);
        return userCountPair;
    }

    @Override
    public List<Review> getUserReviews(UserDto userDto) {
        User user = repository.getByPK(userDto.getId())
                .orElseThrow(RuntimeException::new);
        return user.getReviews();
    }

    @Override
    public List<Tour> getUserTours(UserDto userDto) {
        User user = repository.getByPK(userDto.getId())
                .orElseThrow(RuntimeException::new);
        return user.getTours();
    }

    @Override
    public void addTour(UserDto userDto, Tour tour) {
        User user = repository.getByPK(userDto.getId())
                .orElseThrow(RuntimeException::new);
        user.getTours().add(tour);
        repository.update(user);
    }

    @Override
    public UserDto getByLogin(String login) {
        GetUserByLoginSpecification specification = context.getBean(GetUserByLoginSpecification.class);
        specification.setLogin(login);
        User user = repository.query(specification).get(0);
        return userDtoEntityConverter.toDTO(user);
    }

    @Override
    public UserDto registerNewUserAccount(UserDto userDto) throws LoginAlreadyExistException {
        if (this.getByLogin(userDto.getLogin()) != null) {
            throw new LoginAlreadyExistException(userDto.getLogin());
        } else {
            User user = userDtoEntityConverter.toEntity(userDto);
            user.setRole(Role.USER);
            user = repository.save(user).orElseThrow(RuntimeException::new);
            return userDtoEntityConverter.toDTO(user);
        }
    }

    @Override
    public boolean hasThisTour(UserDto userDto, String tourId) {
        List<Tour> tours = getUserTours(userDto);
        return tours.stream()
                .map((Tour::getId))
                .anyMatch(tourId::equals);
    }

    @Override
    public void removeTourFromUser(UserDto userDto, String tourId) {
        User user = repository.getByPK(userDto.getId()).orElseThrow(RuntimeException::new);
        List<Tour> tours = user.getTours();
        Tour tour = tours.stream().filter(t -> t.getId().equals(tourId)).findFirst().get();
        tours.remove(tour);
        repository.update(user);
    }
}
