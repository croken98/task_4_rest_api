package by.zinkov.task4.service.impl;

import by.zinkov.service.ServiceAbstract;
import by.zinkov.task4.bean.Hotel;
import by.zinkov.task4.pair.PairPaginated;
import by.zinkov.task4.specification.PaginatedSpecification;
import by.zinkov.task4.specification.impl.GetPaginatedHotelsSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelServiceImpl extends ServiceAbstract<Hotel, String> implements by.zinkov.task4.service.HotelService<String> {
    @Autowired
    private ApplicationContext context;

    public PairPaginated<List<Hotel>, Long> getPaginatedHotels(int pageNumber, int countPerPage) {
        PaginatedSpecification<Hotel> specification = context.getBean(GetPaginatedHotelsSpecification.class);
        specification.setCount(countPerPage);
        specification.setPageNumber(pageNumber);
        List<Hotel> hotels = repository.query(specification);
        Long countHotels = repository.getCount();
        PairPaginated<List<Hotel>, Long> hotelsCountPair = new PairPaginated<>();
        hotelsCountPair.setObjects( hotels);
        hotelsCountPair.setCount(countHotels);
        return hotelsCountPair;
    }
}
